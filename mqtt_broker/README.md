## Installation	:

	# Using Docker:
	
	* Docker installation based on https://github.com/jllopis/docker-mosquitto
	* Uses mosquitto MQTT broker with mosquitto auth plugin for client authentication using MongoDB as backend storage. Docker image based on Alpine OS. Dockerfile along with mosquitto.conf, auth-plugin.conf and Makefile included in /mosquitto dir.
	* Requires mongoDB container running in the same network to enable auth plugin. Mongo container based on mvertes/alpine-mongo image.
	* Use docker-compose to start and organise containers.


	# Rock base
	Platform : Ubuntu 14.04/16.04 64 bit server

	Refer this link for detailed installation producedure:
	https://www.digitalocean.com/community/questions/how-to-setup-a-mosquitto-mqtt-server-and-receive-data-from-owntracks

			apt-get update
			apt-get install build-essential libwrap0-dev libssl-dev libc-ares-dev uuid-dev xsltproc
			cd /home/mosquitto
			wget http://mosquitto.org/files/source/mosquitto-1.4.8.tar.gz
			tar xvzf mosquitto-1.4.8.tar.gz
			cd mosquitto-1.4.8
			make
			make install

	Install the appropriate backend developer files (e.g. MongoDB backend)
	Requires mongo-c-driver (https://github.com/mongodb/mongo-c-driver)

		    git clone https://github.com/mongodb/mongo-c-driver
			cd mongo-c-driver/
			# make uninstall 		; if you already installed it
			git checkout r1.2
			./autogen.sh --with-libbson=bundled
			make
			make install
	
	after a successful install, you can proceed with compiling mosquitto-auth-plug but plz be inform to do make clean
	Get mosquitto-auth-plug source and create a suitable configuration file

		    git clone https://github.com/jpmens/mosquitto-auth-plug.git
		    cd mosquitto-auth-plug
		    cp config.mk.in config.mk

	Edit the created config.mk file to suit your needs

    		vi config.mk

    While using MongoDB, use BACKEND_MONGO ?= yes and MOSQUITTO_SRC = /home/user/mosquitto/mosquitto-1.4.8 (dir to mosquitto source file either from tar.gz or git)

	Inside the mosquitto-auth-plug directory use the make command to build the plugin and move it next to mosquitto.conf file

		    make
		    mv auth-plug.so /etc/mosquitto/


	mkdir /var/lib/mosquitto/
	cp /etc/mosquitto/mosquitto.conf.example /etc/mosquitto/mosquitto.conf
	editor /etc/mosquitto/mosquitto.conf

	Change configuration in mosquitto.conf
		listener 8883 <yourIP>	// for cloud, use virtual IP (eg. 10.0.0.4)
		persistence true
		persistence_location /var/lib/mosquitto/
		persistence_file mosquitto.db
		log_dest syslog
		log_dest stdout
		log_dest topic
		log_type error
		log_type warning
		log_type notice
		log_type information
		connection_messages true
		log_timestamp true
		allow_anonymous false
		password_file /etc/mosquitto/pwfile 	// password file generated from mosquitto_passwd

	/sbin/ldconfig
	mosquitto -c /etc/mosquitto/mosquitto.conf -d 	// -d tag for deamon mode

	Update init script to start broker at startup.



## Security :

* Authentication plugin based on https://github.com/jpmens/mosquitto-auth-plug using MongoDB as backend storage

*	(To be implemented . . .)
	SSL encryption at transport layer
	Using LetsEncrypt SSL cerificate
