/*
 * Connect to Wifi using wifi_manager
 * Connect to MQTT broker
 
*/

#include <FS.h>                   //this needs to be first, or it all crashes and burns...
#include <ESP8266WiFi.h>          //https://github.com/esp8266/Arduino
#include <PubSubClient.h>         // library to implement MQTT
#include <DNSServer.h>
#include <WiFiManager.h>          //https://github.com/tzapu/WiFiManager
#include <ESP8266httpUpdate.h>    // for software update
#include <ArduinoJson.h>          // to parse JSON

WiFiManager wifiManager;
WiFiClient espClient;
PubSubClient client(espClient);

// Configuration.
const char* AutoConnectAP = "ESP Smart Switch";
const char* AutoConnectAP_password = "esp@12345";
// default static IP config for captive portal
IPAddress _ip = IPAddress(10, 0, 1, 78);
IPAddress _gw = IPAddress(10, 0, 1, 1);
IPAddress _sn = IPAddress(255, 255, 255, 0);

const char* mqtt_server = "iot.eclipse.org";
const char* roomID = "bufo/iot";


void wifi_setup()
{ 
  wifiManager.setSTAStaticIPConfig(_ip, _gw, _sn);    //set static ip 

  //tries to connect to last known settings
  //if it does not connect it starts an access point with the specified name
  //and goes into a blocking loop awaiting configuration
  if (!wifiManager.autoConnect(AutoConnectAP, AutoConnectAP_password)) {
    Serial.println("failed to connect, resetting device to reconnect");
    delay(3000);
    ESP.reset();
    delay(5000);
  }

  //if you get here you have connected to the WiFi
  Serial.println("connected to wifi");
  Serial.println("local ip");
  Serial.println(WiFi.localIP());
}


void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();

  // Switch on the LED if an 1 was received as first character
  if ((char)payload[0] == '1') {
    digitalWrite(13, LOW);   // Turn the LED on (Note that LOW is the voltage level
    // but actually the LED is on; this is because
    // it is acive low on the ESP-01)
  } else {
    digitalWrite(13, HIGH);  // Turn the LED off by making the voltage HIGH
  }

}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Create a random client ID
    String clientId = "ESP8266Client-";
    clientId += String(random(0xffff), HEX);
    // Attempt to connect
    if (client.connect(clientId.c_str())) {
      Serial.println("connected");
      // Once connected, publish an announcement...
      // client.publish(roomID, "hello world");
      // ... and resubscribe
      client.subscribe(roomID);
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

void parseJSON(char* json)
{
  DynamicJsonBuffer jsonBuffer;
  JsonObject& root = jsonBuffer.parseObject(json);

  // Test if parsing succeeds.
  if (!root.success()) {
    Serial.println("Failed to parse JSON");
    return;
    }
  const char* sensor = root["sensor"];
  long time = root["time"];
  double latitude = root["data"][0];
  double longitude = root["data"][1];
}

void setup() {
  pinMode(13, OUTPUT);     // Initialize the BUILTIN_LED pin as an output
  Serial.begin(115200);
  wifi_setup();
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);
}

void loop() {

  if (!client.connected()) {
    reconnect();
  }
  client.loop();
}
