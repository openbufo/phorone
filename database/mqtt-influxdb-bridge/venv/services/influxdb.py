#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__    = 'Ben Jones <ben.jones12()gmail.com>'
__copyright__ = 'Copyright 2016 Ben Jones'
__license__   = """Eclipse Public License - v 1.0 (http://www.eclipse.org/legal/epl-v10.html)"""

import requests
import logging
import json

# disable info logging in requests module (e.g. connection pool message for every post request)
logging.getLogger("requests").setLevel(logging.WARNING)

def plugin(srv, item):
    ''' addrs: (measurement) '''

    srv.logging.debug("*** MODULE=%s: service=%s, target=%s", __file__, item.service, item.target)

    host        = item.config['host']
    port        = item.config['port']
    username    = item.config['username']
    password    = item.config['password']
    #database    = item.config['database']

    params = item.topic.split("/")
    database = params[1]
    #measurement = item.addrs[0]
    measurement = params[2]
    # iterate through each keys in payload dict
    try:
        tag = ""
        val = ""
        tags = json.loads(item.payload)
        for key, value in tags.items():
            if str(key) == "value":
                val = " value=" + str(value)
                continue
            if tag != "":
                tag = tag + ","
            tag = tag + str(key) + "=" + str(value)
    except Exception as e:
        srv.logging.warn("Failed to parse input payload. Must be json")


    #tag         = "topic=" + item.topic.replace('/', '_')
    #val       = item.payload
    
    try:
        url = "http://%s:%d/write?db=%s" % (host, port, database)
        data = measurement + ',' + tag + val
        
        if username is None:
            r = requests.post(url, data=data)
        else:
            r = requests.post(url, data=data, auth=(username, password))
        
        # success
        if r.status_code == 204:
            return True
            
        # request accepted but couldn't be completed (200) or failed (otherwise)
        if r.status_code == 200:
            srv.logging.warn("POST request could not be completed: %s" % (r.text))
        else:
            srv.logging.warn("POST request failed: (%s) %s" % (r.status_code, r.text))
        
    except Exception as e:
        srv.logging.warn("Failed to send POST request to InfluxDB server using %s: %s" % (url, str(e)))

    return False
