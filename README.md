## Phorone (Server Sided) IoT Platform

 Platform facilitates a smooth, reliable and low cost communication between devices connected to internet. It uses MQTT messaging protocal based on publish/subscribe model to communicate over TCP/IP. Each connected device and server subscribes and publishes data to a topic via MQTT broker. 

Components :
	1. MQTT Broker - Mosquitto docker image with mosquitto-auth-plug for authentication. Uses mongoDB backend for storing user details
	2. MQTT - InfluxDB bridge using mqttwarn
	3. Database - Influxdb
	4. Dashboard/Visualization using Grafana
	5. API - Primarily to be used for internal tasks like sending SMS based notification, elasticsearch annotations etc

Architecture :
Server sided implementation can be distributed into 3 parts:

	---------------------			-----------------------			----------------------
	|					|			|					  |			|					 |
	|	MQTT Broker		|		 	| 		Database	  |      	|   Visualization    |
	|	(Mosquitto)		|	==== >	|  		InfluxDB 	  |	 ==== >	|	   (Grafana)	 |
	|mosquitto-auth-plug|			|MQTT -InfluxDB Bridge|			| 		  API		 |	
	|	  MongoDB		|			|					  |			|					 |
	---------------------			-----------------------			----------------------

Each segments are to be installed in different VMs pulling docker images.

Backups :
Regular incremental snapshots of each VMs.
Multi-zone backups in case of high reliability.
Influxdb backups on AWS Glaciers