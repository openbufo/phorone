from uuid import uuid4
import boto3
from boto3.session import Config
from botocore.exceptions import ClientError
import os.path
from flask import current_app as app
from werkzeug.utils import secure_filename
import requests
import json

ALLOWED_EXTENSIONS = set(['doc', 'docx', 'xls', 'txt', 'pdf', 'png', 'jpg', 'jpeg'])

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

def s3_upload(source_file):
    try:
        source_filename = secure_filename(source_file.filename)
        source_extension = os.path.splitext(source_filename)[1]
        if not allowed_file(source_filename):
            raise(Exception("unsupported file format"))

        destination_filename = uuid4().hex + source_extension

        bucket_name = app.config["S3_BUCKET"]
        print(bucket_name)

        s3 = boto3.resource('s3', config=Config(signature_version='s3v4'))      # using AWS S3 for file storage
        s3.Object(bucket_name, destination_filename).put(Body=source_file,  \
                                            ACL='public-read',  \
                                            Metadata={ 'author': 'binayak'})
        asset_url = app.config["S3_BUCKET_BASEURL"] + str(destination_filename)
        return asset_url
    except ClientError as e:
        print(e)
        return False
    except Exception as ex:
        print(ex)
        return False

def es_upload(payload, elasticIndex, datatype):
    try:
        url = app.config["ELASTIC_URL"] + str(elasticIndex) + "/" + str(datatype)
        print("base url : ",url)
        payload = json.dumps(payload)
        print("payload", payload)
        headers = {
            'content-type': "application/json",
            'cache-control': "no-cache",
        }
        response = requests.request("POST", url, data=payload, headers=headers)
        print(response.text)
        return True
    except Exception as e:
        print(e)
        return False

def es_search(elasticIndex, datatype, searchParam):
    try:
        print("searching ...")
        output = []
        payload = {}
        url = app.config["ELASTIC_URL"] + str(elasticIndex) + "/" + str(datatype) + "/_search"
        if searchParam:
            pagination = searchParam['from'] if ('from' in searchParam and searchParam['from'] in range(0,100)) else 0
            resultSize = searchParam['size'] if ('size' in searchParam and searchParam['size'] in range(0,100)) else 10
            sortOrder = searchParam['sort'] if ('sort' in searchParam and searchParam['size'] in ['asc','desc']) else "desc"
            payload = { "query": {  \
                                    "wildcard" : \
                                    { searchParam['field'] : { "value" : searchParam['text'], "boost" : 2.0 } }\
                        }, \
                        "sort" : [{ searchParam['field'] : {"order" : sortOrder}}],
                        "from" : pagination, \
                        "size" : resultSize
                    }
        headers = {
            'content-type': "application/json",
            'cache-control': "no-cache",
        }
        print("Payload :", payload)
        response = requests.request("POST", url, data=json.dumps(payload), headers=headers)
        response = json.loads(response.text)
        print(response)
        if len(response['hits']['hits']) == 0:
            return {"status":"fail", "text":"No result found"}
        for row in response['hits']['hits']:
            user = row['_source']['user'] if 'user' in row['_source'] else "Anonymous"
            timestamp = row['_source']['post_date'] if 'post_date' in row['_source'] else "2017-01-15"
            message = row['_source']['message'] if 'message' in row['_source'] else ""
            attachment = []
            if 'attachment' in row['_source']:
                if type(row['_source']['attachment']) is list:
                    for link in row['_source']['attachment']:
                        attachment.append({"link":link, "type":link.rsplit('.', 1)[1].lower()})
                    print(output)
                else:
                    attachment.append({"link":row['_source']['attachment'], "type":row['_source']['attachment'].rsplit('.', 1)[1].lower()})
                    output.append({"timestamp":timestamp, "user":user, "message":message, "attachment":attachment})
            else:
                output.append({"timestamp":timestamp, "user":user, "message":message})
        return output
    except Exception as e:
        print(e)
        return False



def connectDB():
    print("trying to connect to database")
    host = 'mongodb://bufoiot:nlhh5zh7@ds143000.mlab.com:43000/bufoiot'
    port = 25469
    db_name = 'bufoiot'
    client = MongoClient(host,port)                # set up Mongo client
    db = client[db_name]                            # select database
    print("connected to database successfully")
    return db


def sendSMS(senderID, mobiles, message):
    authkey = "145933A9wi7lXms58d26a1a" # Your authentication key.
    route = "4"         # Define route
    # Prepare you post parameters
    values = {
              'authkey' : authkey,
              'mobiles' : mobiles,
              'message' : message,
              'sender' : senderID,
              'route' : route
              }
    url = "https://control.msg91.com/api/sendhttp.php" # API URL
    data = bytes( urllib.parse.urlencode(values).encode() )
    handler = urllib.request.urlopen( url, data );
    
