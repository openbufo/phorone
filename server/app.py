from flask import Flask, request, jsonify
from pymongo import MongoClient
import urllib.request # Python URL functions
from urllib.parse import unquote
from werkzeug.utils import secure_filename
from tools import s3_upload, es_upload, es_search, allowed_file, sendSMS, connectDB
import json
	

app = Flask(__name__)
app.config.from_object('config')



# POST /api/listenAlarm
@app.route('/api/listenAlarm', methods=['POST'])
def processAlarm():
	try:
		db = connectDB()
		inputJSON = request.get_json(force=True)		# process the input alert from Kapacitor

		'''{'evalMatches': 
		[{'metric': 'nspcl.generation', 'tags': None, 'value': 49.89}], 
		'state': 'alerting', 
		'ruleName': 'Generation Unit 1 alert', 
		'title': '[Alerting] Generation Unit 1 alert', 
		'ruleId': 1, 
		'message': 'Low generation value'}'''


		if inputJSON['state'] == 'alerting':
			deviceID = inputJSON['evalMatches'][0]['metric'].partition(".")[0]
			message = inputJSON['title'] + "\nStatus : Warning \n" + inputJSON['message'] + "\nCurrent Value : " + inputJSON['evalMatches'][0]['value'] + "\nView dashboard at "
		elif inputJSON['state'] == 'ok':
			# retrieve measurement value from the Grafana API
			alertID = inputJSON['ruleId']
			# bearer token : eyJrIjoieXEzWDN3cGMzSFRVMUExSlBscnJhUE5yNmpRWk1VdEUiLCJuIjoiZGV2ZWxvcGVyIiwiaWQiOjJ9
			# http://bufoiot.cloudapp.net/api/alerts/2
			
			measurement = json_from_api['Settings']["conditions"][0]['query']['model']['measurement']
			message = inputJSON['title'] + "\nStatus : OK " + "\nCurrent Value : " + inputJSON['evalMatches'][0]['value'] + "\nView dashboard at "
		else:
			raise Exception("no alarm received")

		# query all user associated to all the groups linked to device, avoiding duplicate records
		users = db['groups'].aggregate([	\
		{ "$lookup" : {	\
				"from" : "devices",	\
		  		"localField" : "_id",	\
		      	"foreignField" : "groups.group_id",	\
		    	"as" : "devices"	\
				}	\
		},	\
		{ "$match" : { "$and" : [{"devices.tag" : deviceID} , {"users" : { "$exists" : True, "$ne": [] }}  ]} }, \
		{ "$unwind" : "$users"},	\
		{ "$match" : {"users.notification" : "enabled"} },	\
		{ "$lookup" : {	\
		  		"from" : "users",	\
		  		"localField" : "users.user_id",	\
		       	"foreignField" : "_id",	\
		        "as" : "user_info"	\
				}	\
		},	\
		{"$unwind" : "$user_info"},	\
		{"$project" : {"user_info.mobile" : 1}},	\
		{"$group": {"_id" : "null", "mobile" : {"$addToSet" : "$user_info.mobile"}}}	\
		])	\


		senderID = "iNSPCL"
		mobiles = ""		# fetch the list of number in the group from db
		#for user in db['groups'].find({ "$and" : [{"group_id" : groupID } , {"alert" : "enabled"}] })
		for mob in users['mobile']:
			mobiles += str(mob)
			mobiles += str(",")
		sendSMS(senderID, mobiles, message)						# POST request to SMS Gateway (msg91)
		return jsonify({'status':'success'})
	except Exception as e:
		print(e)
		return jsonify({'status': 'fail', 'error':'Invalid input paramaters or format', 'error_text':e})



# POST /api/manageUser
# add / delete user to a group
@app.route('/api/user/<action>', methods=['POST'])
def manageUser(action):
	try:
		inputJSON = request.get_json(force=True)		# process input to API 
		mobile = inputJSON['mobile']
		inputMessage = unquote(unquote(inputJSON['message']))
		keyword = inputJSON['keyword']
		groupTag = inputMessage.split()[1]
		group = db['groups'].find({"tag":groupTag})[0]
		senderID = "%.*s" % (6, (groupTag + str("LLLLLL")))
		if action == 'add':
			group[0].insert({"mobile" : mobile, "alarm" : "enabled"})
			successMsg = "You have been added to alarm group " + str("groupTag") +".To stop receiving alarms from this group, reply with UNSUBALARM " + str(groupTag) 
			sendSMS(senderID, mobile, message)
		elif action == "delete":
			# to remove user from the group
			group.update({"mobile" : mobile}, {"alarm" : "disabled"}, upsert = True)
			successMsg = "You have been added to alarm group " + str("groupTag") +".To stop receiving alarms from this group, reply with UNSUBALARM " + str(groupTag) 
			sendSMS(senderID, mobile, message)
		return jsonify({'status':'success'})
	except Exception as e:
		print(e)
		return jsonify({'status': 'fail', 'error':'Invalid input paramaters or format'})


@app.route('/getAnnotations', methods = ['GET', 'POST'])
def getAnnotations():
	try:
		if request.method == 'POST':
			paramJSON = request.get_json(force=True)
			elasticIndex = paramJSON['org']
			print(elasticIndex)
			datatype = "annotation"
			search = {}
			if 'search' in paramJSON:
				search['field'] = paramJSON['search']['field']
				search['text'] = paramJSON['search']['text']
				search['size'] = paramJSON['search']['size'] if 'size' in paramJSON['search'] else 10
				search['from'] = paramJSON['search']['from'] if 'from' in paramJSON['search'] else 0
				search['sort'] = paramJSON['search']['sort'] if 'sort' in paramJSON['search'] and paramJSON['search'] in ['asc','desc'] else 'desc'

			result = es_search(elasticIndex, datatype, search)
			if result:
				return jsonify(result)
		return jsonify({'status': 'fail', 'text':'Something went wrong. Try Again'})
	except Exception as e:
		return jsonify({"status":"fail","text":str(e)})


@app.route('/annotate', methods = ['GET', 'POST'])
def annotate():
	try:
		if request.method == 'POST':
			# check if label is available
			if 'label' not in request.form:
				return jsonify({"status":"fail","text":"Label is mandatory"})
			payload = {}
			payload["label"] = request.form['label']
			payload["user"] = request.form['user']
			elasticIndex = request.form['org']
			datatype = "annotation"

			if 'message' in request.form and request.form['message'] != '':
				payload['message'] = request.form['message']

			if 'file' in request.files and request.files['file'] != "":
				file = request.files['file']

			if 'tags' in request.form and request.form['tag'] != "":
				payload['tags'] = request.form['tags']

			if file and allowed_file(file.filename):
				asset_url = s3_upload(source_file=file) # on successful upload, return the download url of the file
			
			# add record in elastic
			payload['attachment'] = asset_url
			status = es_upload(payload, elasticIndex, datatype)
			if status:
				return jsonify({"status":"success" })
		return jsonify({"status":"fail","text":"Something went wrong. Try again."})
	except Exception as e:
		return jsonify({"status":"fail","text":str(e)})


if __name__ == '__main__':
	app.run(debug=True)
	



